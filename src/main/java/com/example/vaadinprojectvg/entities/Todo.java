package com.example.vaadinprojectvg.entities;

import javax.persistence.*;

@Entity
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String todoDescription;

    @Column
    private boolean isCompleted;

    @ManyToOne
    @JoinColumn(name = "appUser_id")
    private AppUser appUser;

    public Todo(String todoDescription, AppUser appUser) {
        this.todoDescription = todoDescription;
        this.appUser = appUser;
        this.isCompleted = false;
    }

    public Todo() {
    }

    public int getId() {
        return id;
    }

    public String getTodoDescription() {
        return todoDescription;
    }

    public void setTodoDescription(String todoDescription) {
        this.todoDescription = todoDescription;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }
}
