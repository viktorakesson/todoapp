package com.example.vaadinprojectvg.components;


import com.example.vaadinprojectvg.entities.Todo;
import com.example.vaadinprojectvg.services.TodoService;
import com.example.vaadinprojectvg.views.TodoView;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;

public class TodoForm extends FormLayout {

    TextField todoDescription = new TextField("Todo", "Write Todo!");
    Button saveButton = new Button("Save");

    Binder<Todo> binder = new BeanValidationBinder<>(Todo.class);
    TodoService todoService;
    TodoView todoView;

    public TodoForm(TodoView todoView, TodoService todoService) {
        this.todoView = todoView;
        this.todoService = todoService;
        binder.bindInstanceFields(this);

        setVisible(false);

        saveButton.addClickListener(e -> handleSave());
        saveButton.addClickShortcut(Key.ENTER);

        add(todoDescription, saveButton);

    }

    public void handleSave(){
        Todo todo = binder.validate().getBinder().getBean();
        if(todo.getId() == 0) {
            todoService.save(todo);
        } else {
            todoService.updateById(todo.getId(), todo);
        }
        todoView.updateTodos();
        setTodo(null);

        this.getParent().ifPresent(component -> {
            if(component instanceof Dialog){
                ((Dialog) component).close();
            }
        });

    }

    public void setTodo(Todo todo){
        if(todo != null) {
            binder.setBean(todo);
            setVisible(true);
        } else {
            setVisible(false);
        }
    }

}
