package com.example.vaadinprojectvg.services;

import com.example.vaadinprojectvg.entities.Todo;
import com.example.vaadinprojectvg.repositories.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {

    @Autowired
    TodoRepository todoRepository;

    public List<Todo> findAll(){
        return todoRepository.findAll();
    }

    public List<Todo> findAllCompleted(){
        return todoRepository.findAll().stream().filter(Todo::isCompleted).collect(Collectors.toList());
    }

    public List<Todo> findAllNotCompleted(){
        return todoRepository.findAll().stream().filter(todo -> !todo.isCompleted()).collect(Collectors.toList());
    }

    public void save(Todo todo){
        todoRepository.save(todo);
    }

    public void deleteById(int id) {
        todoRepository.deleteById(id);
    }

    public void updateById(int id, Todo todo) {
        Todo newTodo = todoRepository.findById(id).orElseThrow();

        if(todo.getTodoDescription() != null){
            newTodo.setTodoDescription(todo.getTodoDescription());
            newTodo.setCompleted(todo.isCompleted());
        }

        todoRepository.save(newTodo);
    }

    public List<Todo> findByUsername(String username) {
        return todoRepository.findByAppUser_Username(username);
    }
}
