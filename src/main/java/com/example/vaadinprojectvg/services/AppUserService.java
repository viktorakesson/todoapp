package com.example.vaadinprojectvg.services;

import com.example.vaadinprojectvg.entities.AppUser;
import com.example.vaadinprojectvg.repositories.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AppUserService {

    @Autowired
    AppUserRepository appUserRepository;

    public void save(AppUser appUser){
        appUserRepository.save(appUser);
    }

    public AppUser findFirst(){
        return appUserRepository.findAll().stream().findFirst().orElseThrow();
    }

    public Optional<AppUser> findByUsername(String username) {
        return appUserRepository.findAppUserByUsername(username);
    }


}
