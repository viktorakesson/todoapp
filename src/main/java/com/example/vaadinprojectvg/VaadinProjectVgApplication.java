package com.example.vaadinprojectvg;

import com.example.vaadinprojectvg.entities.AppUser;
import com.example.vaadinprojectvg.entities.Todo;
import com.example.vaadinprojectvg.repositories.AppUserRepository;
import com.example.vaadinprojectvg.repositories.TodoRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class VaadinProjectVgApplication {

    public static void main(String[] args) {
        SpringApplication.run(VaadinProjectVgApplication.class, args);
    }

    /*
    @Bean
    CommandLineRunner init(TodoRepository todoRepository, AppUserRepository appUserRepository) {
        return args -> {

            AppUser appUser = new AppUser("Gunnar", "pw");
            AppUser appUser1 = new AppUser("Kalle", "pw");
            appUserRepository.save(appUser);
            appUserRepository.save(appUser1);

            Todo todo = new Todo("Persistent save", appUser);
            Todo todo2 = new Todo("Lös multipla forms", appUser1);
            todoRepository.save(todo);
            todoRepository.save(todo2);

        };
    }

     */

}
