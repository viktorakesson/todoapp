package com.example.vaadinprojectvg.views;

import com.example.vaadinprojectvg.components.TodoForm;
import com.example.vaadinprojectvg.entities.Todo;
import com.example.vaadinprojectvg.security.SecurityService;
import com.example.vaadinprojectvg.services.AppUserService;
import com.example.vaadinprojectvg.services.TodoService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;


import javax.annotation.security.PermitAll;

@Route(value = "/", layout = AppView.class)
@PermitAll
@Theme(themeClass = Lumo.class, variant = Lumo.DARK)
public class TodoView extends VerticalLayout implements AppShellConfigurator {

    Grid<Todo> todoGrid = new Grid<>(Todo.class, false);
    TodoForm todoForm;
    TodoService todoService;
    AppUserService appUserService;

    public TodoView(TodoService todoService, AppUserService appUserService){
        this.todoService = todoService;
        this.appUserService = appUserService;
        this.todoForm = new TodoForm(this, todoService);

        setGrid();
        setWidthFull();
        setJustifyContentMode(JustifyContentMode.CENTER);
        setAlignItems(Alignment.CENTER);

        Span name = new Span(SecurityService.getPrincipalName());
        H3 welcome = new H3("Välkommen till dina Todos ");
        welcome.add(name);

        add(welcome);
        HorizontalLayout todoContainer = new HorizontalLayout();

        todoContainer.add(todoGrid, todoForm);
        todoContainer.setWidth("70%");

        Button addTodoButton = new Button("Add Todo", e -> {
            Dialog dialog = new Dialog();
            TodoForm todoForm = new TodoForm(this, todoService);
            Todo todo = new Todo();
            todo.setAppUser(appUserService.findByUsername(SecurityService.getPrincipalName()).orElseThrow());
            todo.setCompleted(false);
            todoForm.setTodo(todo);
            dialog.setDraggable(true);
            dialog.add(todoForm);
            dialog.open();
        });

        add(todoContainer, addTodoButton);

    }

    private void setGrid() {

        todoGrid.setItems(todoService.findByUsername(SecurityService.getPrincipalName()));
        todoGrid.addColumn(todo -> todo.getAppUser().getUsername()).setHeader("Added by").setResizable(true);
        todoGrid.addColumn(Todo::getTodoDescription).setHeader("Description").setWidth("50%");

        todoGrid.addComponentColumn(todo -> {
            Checkbox checkbox = new Checkbox(todo.isCompleted());
            checkbox.addValueChangeListener(e -> {
                todo.setCompleted(!todo.isCompleted());
                todoService.updateById(todo.getId(), todo);
                updateTodos();
            });
            return checkbox;
        }).setHeader("Done");

        todoGrid.addComponentColumn(todo -> {
            Button deleteButton = new Button("Delete", e -> {
                todoService.deleteById(todo.getId());
                updateTodos();
            });
            return deleteButton;
        });

        todoGrid.asSingleSelect().addValueChangeListener(e -> {
            todoForm.setTodo(e.getValue());
        });

    }

    public void updateTodos() {
        todoGrid.setItems(todoService.findByUsername(SecurityService.getPrincipalName()));
    }

}
