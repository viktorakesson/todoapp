package com.example.vaadinprojectvg.views;

import com.example.vaadinprojectvg.security.SecurityService;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.BoxSizing;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import org.springframework.beans.factory.annotation.Autowired;

public class AppView extends AppLayout {

    @Autowired
    SecurityService securityService;

    public AppView (SecurityService securityService) {
        this.securityService = securityService;

        HorizontalLayout header = new HorizontalLayout();

        H1 logo = new H1("Do | Todos");
        Button loginButton = new Button("Logout", e -> securityService.logout());

        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        header.setJustifyContentMode(FlexComponent.JustifyContentMode.EVENLY);
        header.setWidthFull();

        header.add(logo, loginButton);

        addToNavbar(header);

    }
}
